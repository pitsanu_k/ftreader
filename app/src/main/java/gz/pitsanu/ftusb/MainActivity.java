package gz.pitsanu.ftusb;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.feitian.readerdk.Tool.DK;
import com.feitian.readerdk.Tool.Tool;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import gz.pitsanu.ftusb.apdu.IAPDU_THAI_IDCARD;
import gz.pitsanu.ftusb.apdu.THAI_IDCARD_3B67;
import gz.pitsanu.ftusb.apdu.THAI_IDCARD_3B68;
import gz.pitsanu.ftusb.ft.FtBlueReadException;
import gz.pitsanu.ftusb.ft.FtReader;
import gz.pitsanu.ftusb.ft.STATUS;
import gz.pitsanu.ftusb.models.IdCard;
import gz.pitsanu.ftusb.models.PhotoCommand;

public class MainActivity extends AppCompatActivity implements Handler.Callback {
    private static final String ACTION_USB_PERMISSION = "gz.pitsanu.ftusb.USB_PERMISSION";

    @BindView(R.id.BList) Button mList;
    @BindView(R.id.spinner) Spinner mSpinner;
    @BindView(R.id.BConnect) Button mConnect;
    @BindView(R.id.BDisconnect) Button mDisconnect;
    @BindView(R.id.BPowerOn) Button mPowerOn;
    @BindView(R.id.BPowerOff) Button mPowerOff;

    @BindView(R.id.photo) ImageView mPhoto;
    @BindView(R.id.TId) TextView mId;
    @BindView(R.id.TNameTh) TextView mNameTH;
    @BindView(R.id.TNameEn) TextView mNameEN;
    @BindView(R.id.TBirthdate) TextView mBirthdate;
    @BindView(R.id.TGender) TextView mGender;
    @BindView(R.id.TAddress) TextView mAddress;
    @BindView(R.id.TIssue) TextView mIssue;
    @BindView(R.id.TExpire) TextView mExpire;
    @BindView(R.id.TLog) TextView mLog;

    private ArrayAdapter<String> mAdapter;
    private UsbManager mUsbManager;

    private PendingIntent mPermissionIntent;
    private FtReader mReader;
    private IAPDU_THAI_IDCARD apduCard;
    private Handler mHandler;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mLog.setMovementMethod(new ScrollingMovementMethod());

        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<String>());
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mAdapter);

        mSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {}

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}
        });

        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);

        stat_close();
        mHandler = new Handler(Looper.getMainLooper(), this);
    }

    private void stat_close() {
        mList.setFocusable(true);
        mConnect.setEnabled(false);
        mDisconnect.setEnabled(false);
        mPowerOn.setEnabled(false);
        mPowerOff.setEnabled(false);
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            stat_close();

            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                onListClicked(null);

                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    logMsg("Add:  DeviceName:  " + device.getDeviceName()
                            + "  DeviceProtocol: " + device.getDeviceProtocol()
                            + "\n");
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                setInfoData(new IdCard());
                onDisconnect(null);
                mList.setEnabled(false);
                mLog.setText("");

                if (mReader != null) {
                    mReader.close();
                }

                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    logMsg("Del: DeviceName:  " + device.getDeviceName()
                            + "  DeviceProtocol: " + device.getDeviceProtocol()
                            + "\n");
                }
            }
        }
    };

    public void onListClicked(View v) {
        mAdapter.clear();
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        for (UsbDevice device : deviceList.values()) {
            mAdapter.add(device.getDeviceName());
            mConnect.setEnabled(true);
            mDisconnect.setEnabled(false);
        }
    }

    public void onConnect(View v) {
        String selectedDevice = (String) mSpinner.getSelectedItem();
        String deviceName;
        for (UsbDevice device : mUsbManager.getDeviceList().values()) {
            deviceName = device.getDeviceName();
            if (deviceName.equals(selectedDevice)) {
                if (mReader != null) {
                    mReader.close();
                }

                if (!mUsbManager.hasPermission(device)) {
                    mUsbManager.requestPermission(device, mPermissionIntent);
                }

                if (!mUsbManager.hasPermission(device)) {
                    return;
                }

                mReader = new FtReader(mUsbManager, device);
                break;
            }
        }

        connectReader();
    }

    private void connectReader() {
        try {
            mReader.open();

            stat_close();
            mDisconnect.setEnabled(true);
            mPowerOn.setEnabled(true);

            byte ReaderVersion[] = new byte[512];
            int[] len = new int[1];
            mReader.getVersion(ReaderVersion, len);
            mReader.startCardStatusMonitoring(mHandler);

            logMsg("open success ");
            logMsg("ManufacturerName: " + mReader.getManufacturerName());
            logMsg("Reader: " + mReader.getReaderName());
            logMsg("DK Version:" + mReader.getDkVersion());
            logMsg("Reader Version:" + ReaderVersion[0] + "." + ReaderVersion[1]);
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
        }
    }

    public void onDisconnect(View v) {
        try {
            mReader.close();
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
        }

        stat_close();
        mConnect.setEnabled(true);
    }

    public void getStatus(View v) {
        int ret = mReader.getCardStatus();
        if (STATUS.CCID_ICC_PRESENT_ACTIVE == ret) {
            logMsg("Card is present and is activated");
        } else if (STATUS.CCID_ICC_PRESENT_INACTIVE == ret) {
            logMsg("Card is present and is not activated");
        } else if (STATUS.CCID_ICC_ABSENT == ret) {
            logMsg("Card is absent");
        } else {
            logMsg("Card is UNKNOWN");
        }
    }

    public void getSerialNumber(View v) {
        byte serialNum[] = new byte[128];
        int serialLen[] = new int[1];
        mReader.getSerialNum(serialNum, serialLen);
        serialNum[serialLen[0]] = '\0';
        String str = new String(serialNum);
        logMsg("GetSerialNum-->" + str);
    }

    public byte[] getAttr(View v) {
        try {
            return mReader.getAtr();
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
            return null;
        }
    }

    public void getKeySn(View v) {
        try {
            byte[] buffer = new byte[74];
            int[] bufferLen = new int[1];
            int result = mReader.getKeySn(buffer, bufferLen);
            if (result == 0) {
                logMsg("buffer: " + Tool.byte2HexStr(buffer, buffer.length));
//                logMsg("buffer str: " + new String(buffer, "TIS-620"));
                logMsg("bufferLen: " + bufferLen[0]);
            } else {
                logMsg("getKeySn error!");
            }
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
        }
    }

    public void initDukpt(View v) {
        try {
            byte[] encBuf = new byte[48];
            int result = mReader.initDukpt(encBuf, 48);
            if (result == 0) {
                logMsg("initDukpt: success");
            } else {
                logMsg("initDukpt error!");
            }
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
        }
    }

    public void powerOn(View v) {
        try {
            int ret = mReader.PowerOn();
            if (STATUS.RETURN_SUCCESS == ret) {
                mPowerOff.setEnabled(true);
                mPowerOn.setEnabled(false);
                logMsg("powerOn success !");
            } else {
                logMsg("powerOn error !");
            }
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
        }
    }

    public void powerOff(View v) {
        try {
            int ret = mReader.PowerOff();
            if (STATUS.RETURN_SUCCESS == ret) {
                mPowerOff.setEnabled(false);
                mPowerOn.setEnabled(true);
                logMsg("PowerOff success !");
            } else {
                logMsg("PowerOff error !");
            }
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
        }
    }

    private IdCard readAll() {
        try {
            byte[] array = new byte[256];
            SendCommand(apduCard.CMD_SELECT(), array);
            IdCard card = new IdCard();

            int ret;
            ret = SendCommand(apduCard.CMD_CID(), array); //mReader.transApdu(iApdu.length, arrayOfByte, receiveln, array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setId(new String(array, "TIS-620"));
            }

            ret = SendCommand(apduCard.CMD_PERSON_INFO(), array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setInfo(new String(array, "TIS-620"));
            }

            ret = SendCommand(apduCard.CMD_ADDRESS(), array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setAddress(new String(array, "TIS-620"));
            }

            ret = SendCommand(apduCard.CMD_CARD_ISSUE_EXPIRE(), array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setIssueExpire(new String(array, "TIS-620"));
            }

            byte[] photo = SendPhotoCommand();
            card.setPhoto(photo);

            return card;
        } catch (Exception e) {
            logMsg("Exception: => " + e.toString());
        }

        return null;
    }

    private void setInfoData(IdCard card) {
        mId.setText(card.getId());
        mNameTH.setText(card.getInfoTh());
        mNameEN.setText(card.getInfoEn());
        mBirthdate.setText(card.getBirthdate());
        mGender.setText(card.getSex());
        mAddress.setText(card.getAddress());
        mIssue.setText(card.getIssue());
        mExpire.setText(card.getExpire());

        byte[] photo = card.getPhoto();
        if (photo != null && photo.length > 0) {
            mPhoto.setImageBitmap(BitmapFactory.decodeByteArray(photo, 0, photo.length));
        } else {
            mPhoto.setImageResource(R.drawable.common_ic_googleplayservices);
        }
    }

    public void clearLog(View v) {
        mLog.setText("");
    }

    private int SendCommand(byte[][] commands, byte[] pbRecvBuffer) throws FtBlueReadException {
        int[] receiveln = new int[2];
        int result = -1;
        for (byte[] command : commands) {
            result = mReader.transApdu(command.length, command, receiveln, pbRecvBuffer);
        }

        return result;
    }

    private byte[] SendPhotoCommand() throws FtBlueReadException {
        ArrayList<PhotoCommand> photoCommands = apduCard.GET_CMD_CARD_PHOTO();
        byte[] commandBytes, recv;

        int[] receiveln = new int[2];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        for (PhotoCommand command : photoCommands) {
            recv = new byte[256];
            commandBytes = command.getCmd1();
            mReader.transApdu(commandBytes.length, commandBytes, receiveln, recv);
            if (recv.length > 0) {
                recv = new byte[256];
                commandBytes = command.getCmd2();
                mReader.transApdu(commandBytes.length, commandBytes, receiveln, recv);
                bos.write(recv, 0, recv.length - 2);
            }
        }

        return bos.toByteArray();
    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what != DK.CARD_STATUS)
            return false;

        switch (msg.arg1) {
            case DK.CARD_ABSENT:
                logMsg("IFD card absent");
                removeCard();
                return true;
            case DK.CARD_PRESENT:
                logMsg("IFD card persent");
                initCard();
                return true;
            case DK.CARD_UNKNOWN:
                logMsg("IFD card unknown");
                return true;
            case DK.IFD_COMMUNICATION_ERROR:
                logMsg("IFD IFD error");
                return true;
        }

        return false;
    }

    private void initCard() {
        try {
            powerOn(null);
            byte[] atr = mReader.getAtr();
            apduCard = initApdu(atr);

            new AsyncTask<Void, Void, IdCard>() {
                @Override
                protected void onPreExecute() {
                    mProgressDialog = ProgressDialog.show(MainActivity.this, null, "Getting card data...", true);
                }

                @Override
                protected IdCard doInBackground(Void... params) {
                    return readAll();
                }

                @Override
                protected void onPostExecute(IdCard card) {
                    setInfoData(card);
                    mProgressDialog.dismiss();
                    logMsg("receive: " + card.toString());
                }
            }.execute();

        } catch (FtBlueReadException e) {
            e.printStackTrace();
        }
    }

    private void removeCard() {
        setInfoData(new IdCard());
        powerOff(null);
        mLog.setText("");
    }

    private IAPDU_THAI_IDCARD initApdu(byte[] atr) throws FtBlueReadException {
        if (atr[0] == 0x3B && (atr[1] == 0x68 || atr[1] == 0x78)) {
            return new THAI_IDCARD_3B68();
        } else if (atr[0] == 0x3B && atr[1] == 0x67) {
            return new THAI_IDCARD_3B67();
        } else {
            logMsg("Card not support");
            throw new FtBlueReadException("Card not support");
        }
    }

    private void logMsg(String msg) {
        mLog.append(String.format("\n%s", msg));
        if (mLog.getLineCount() > 25) {
            mLog.scrollTo(0, (mLog.getLineCount() - 25) * mLog.getLineHeight());
        }
    }
}
